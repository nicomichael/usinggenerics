package za.co.nico.using.generics;

/**
 * 
 * @param <X> Bounded type parameter that must be a number
 * @param <Y> Bounded type parameter that must be a number
 * 
 */
public class GenericPoint<X extends Number, Y extends Number> {

    private X xCoord;
    private Y yCoord;

    public GenericPoint(X x, Y y) {
        this.xCoord = x;
        this.yCoord = y;
    }
    
    
    
    

    public X getxCoord() {
        return xCoord;
    }

    public void setxCoord(X xCoord) {
        this.xCoord = xCoord;
    }

    public Y getyCoord() {
        return yCoord;
    }

    public void setyCoord(Y yCoord) {
        this.yCoord = yCoord;
    }

}
