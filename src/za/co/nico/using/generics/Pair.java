
package za.co.nico.using.generics;

public class Pair<K,V> {
    private K key;
    private V value;
    
    public Pair(K key,V value) {
        this.key = key;
        this.value = value;
    }   
    
    public <K,V> boolean compare(Pair<K,V> pair){
        return key.equals(pair.getKey())&& value.equals(pair.getValue()) ;
    }
    
    /**
     * 
     * @param p1 as long as value is a Number or higher (Object) it will be accepted
     *                      value will not be accepted for Integer
     *                      super is a lower bound in the class hierarchy  >= 
     * @return 
     */
    public String wildString(Pair<String,? super Number> p1){
        return p1.getKey();
    }
    
    /**
     * 
     * @param p1 as long as value is a Integer or Number or Object  it will be accepted
     *                      extends is a higher upper bound in the class hierarchy <=
     * @return 
     */
    public String wildString2(Pair<String,? extends Number> p1){
        return p1.getKey();
    }
    
    /**
     * 
     * @param p1 ? is unbound wild card type
     * @return 
     */
    public String wildString3(Pair<String,?> p1){
        return p1.getKey();
    }
    
    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    
    
    
}
