package za.co.nico.using.generics;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    
    public static Pair<String,Integer> pair1=new Pair<>("pair#1",5);
    public static Pair<String,Integer> pair2=new Pair<>("pair#2",15);
    public static Pair<String,Object> pair3=new Pair<>("pair#3",new Object());
    
    public static GenericPoint<Integer,Float> point1=new GenericPoint<>(5,3.414f);
    public static GenericPoint<Integer,Float> point2=new GenericPoint<>(10,6.828f);
            
    private static final long SECONDS=1000;

    public static void main(String[] args) {
        System.out.println("pair1=pair2 : "+pair1.compare(pair2));
        System.out.println("yCoord= "+point1.getyCoord());
        System.out.println("pair3 wildstring  : "+pair3.wildString(pair3));
        waiting(1*SECONDS);
        System.out.println("pair2 wildstring2 : "+pair2.wildString2(pair2));
        waiting(1*SECONDS);
        System.out.println("pair2 wildstring3 : "+pair2.wildString3(pair2));
        waiting(1*SECONDS);
        System.out.println("pair3 wildstring3 : "+pair3.wildString3(pair3));
    }
    
    public static void waiting(long miliseconds){
        try {
            Thread.sleep(miliseconds);
        } catch (InterruptedException ex) {       }
    }
    
}
